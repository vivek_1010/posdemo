<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Script extends MY_Controller
{
function __construct()
    {
        parent::__construct();
        $this->load->library('multipledb');
        $this->load->model('msync_model');
        
    }
   function importdata()
    {   
        $this->msync_model->Sync_to_pos_table();
    }
  function exportdata(){
      
       $this->msync_model->Sync_to_intermediate_table();
  }
}
?>
